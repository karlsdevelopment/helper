<?php

namespace Karls\Helper\Tests;

use Karls\Helper\HelperServiceProvider;

class TestCase extends \Tests\TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        // additional setup
    }

    protected function getPackageProviders($app): array
    {
        return [
            HelperServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        // perform environment setup
    }
}