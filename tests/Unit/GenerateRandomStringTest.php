<?php
declare(strict_types=1);

namespace Karls\Helper\Tests\Unit;

use Exception;
use Karls\Helper\Tests\TestCase;
use TypeError;

class GenerateRandomStringTest extends TestCase
{
    /**
     * @dataProvider data
     * @throws Exception
     */
    public function test_generateRandomString($input, $excepted)
    {
        self::assertSame($excepted, strlen(generateRandomString($input)));
    }

    /**
     * @dataProvider incorrect_data
     * @throws Exception
     */
    public function test_generateRandomString_incorrect_data($input)
    {
        $this->expectException(TypeError::class);
        generateRandomString($input);
    }

    /**
     * @throws Exception
     */
    public function test_generateRandomString_with_error_protection()
    {
        $randomString = generateRandomString(errorProtected: true);
        self::assertTrue(preg_match('/(0)(1)(5)(a)(b)(c)(d)(e)(f)(g)(h)(i)(j)(k)(l)(m)(n)(o)(p)(q)(r)(s)(t)(u)(v)(w)(x)(y)(z)(I)(J)(O)(S)/',
            $randomString) === 0);
    }

    public function data(): array
    {
        return [
            [30, 30],
            [32, 32],
            [0, 0],
            [-1, 0],
        ];
    }

    public function incorrect_data(): array
    {
        return [
            [null],
            ['teststring'],
            [false],
            ['34'],
        ];
    }
}
