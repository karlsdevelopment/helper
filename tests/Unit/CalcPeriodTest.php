<?php
declare (strict_types=1);

namespace Karls\Helper\Tests\Unit;

use Karls\Helper\Tests\TestCase;

class CalcPeriodTest extends TestCase
{
    public function test_calcPeriod_no_period()
    {
        self::assertSame([0, 0], calcPeriod(0));
    }

    /**
     * @dataProvider data
     */
    public function test_calcPeriod_valid_data($input, $expected)
    {
        self::assertSame($expected, calcPeriod($input));
    }

    public function data(): array
    {
        return [
            [1, [0, 1]],
            [10, [0, 10]],
            [12, [1, 0]],
            [13, [1, 1]],
            [24, [2, 0]],
            [25, [2, 1]],
            [200, [16, 8]]
        ];
    }
}