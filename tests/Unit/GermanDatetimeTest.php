<?php
declare(strict_types=1);

namespace Karls\Helper\Tests\Unit;

use Exception;
use JetBrains\PhpStorm\Pure;
use Karls\Helper\Tests\TestCase;
use stdClass;
use TypeError;

class GermanDatetimeTest extends TestCase
{
    /**
     * @dataProvider correct_dates
     * @throws Exception
     */
    public function test_germanDatetime_correct($input, $excepted)
    {
        self::assertSame($excepted, germanDatetime($input));
    }

    /**
     * @dataProvider incorrect_types
     * @throws Exception
     */
    public function test_germanDatetime_incorrect_type($input)
    {
        $this->expectException(TypeError::class);

        germanDatetime($input);
    }

    /**
     * @dataProvider incorrect_dates
     * @throws Exception
     */
    public function test_germanDatetime_incorrect($input)
    {
        $this->expectException(Exception::class);

        germanDatetime($input);
    }

    /**
     * @dataProvider incorrect_dates
     * @throws Exception
     */
    public function test_germanDatetime_incorrect_not_strict($input)
    {
        self::assertSame('', germanDatetime($input, false));
    }

    public function correct_dates(): array
    {
        return [
            ['2021-04-12', '12.04.2021 00:00'],
            ['1890-01-01', '01.01.1890 00:00'],
            ['2034-12-01 9:12', '01.12.2034 09:12'],
            ['2034-12-01 24:55', '02.12.2034 00:55'],
            ['2034/12/01 24:55', '02.12.2034 00:55'],
        ];
    }

    public function incorrect_dates(): array
    {
        return [
            ['0000-00-00'],
            ['2034-12-01 25:55'],
            ['testString']
        ];
    }

    #[Pure] public function incorrect_types(): array
    {
        return [
            [2],
            [true],
            [[1, 2, 3]],
            [new stdClass()]
        ];
    }
}
