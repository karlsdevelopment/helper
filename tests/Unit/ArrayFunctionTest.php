<?php
declare (strict_types=1);

namespace Karls\Helper\Tests\Unit;

use Karls\Helper\Tests\TestCase;

class ArrayFunctionTest extends TestCase
{
    public function test_mask_regular()
    {
        self::assertSame(array_mask(
            [
                'a' => 1,
                'b' => 2,
                'c' => 3,
                'd' => 4,
                'e' => 5,
            ], ['b', 'c', 'e']),
            [
                'b' => 2,
                'c' => 3,
                'e' => 5,
            ]);
    }

    public function test_mask_more()
    {
        self::assertSame(array_mask(
            [
                'a' => 1,
                'b' => 2,
                'c' => 3,
                'd' => 4,
                'e' => 5,
            ], ['b', 'c', 'e', 'f']),
            [
                'b' => 2,
                'c' => 3,
                'e' => 5,
            ]);
    }

    public function test_mask_other()
    {
        self::assertSame(array_mask(
            [
                'a' => 1,
                'b' => 2,
                'c' => 3,
                'd' => 4,
                'e' => 5,
            ], ['f', 'g']),
            []);
    }

    public function test_mask_empty()
    {
        self::assertSame(array_mask([], ['f', 'g']), []);
    }

    public function test_mask_none()
    {
        self::assertSame(array_mask(
            [
                'a' => 1,
                'b' => 2,
                'c' => 3,
                'd' => 4,
                'e' => 5,
            ], []),
            []);
    }

    public function test_mask_double()
    {
        self::assertSame(array_mask(
            [
                'a' => 1,
                'b' => 2,
                'c' => 3,
                'd' => 4,
                'e' => 5,
            ], ['a', 'a']),
            [
                'a' => 1,
            ]);
    }

    public function test_without_regular()
    {
        self::assertSame(array_without(
            [
                'a' => 1,
                'b' => 2,
                'c' => 3,
                'd' => 4,
                'e' => 5,
            ], ['b', 'c', 'e']),
            [
                'a' => 1,
                'd' => 4,
            ]);
    }

    public function test_without_more()
    {
        self::assertSame(array_without(
            [
                'a' => 1,
                'b' => 2,
                'c' => 3,
                'd' => 4,
                'e' => 5,
            ], ['b', 'c', 'e', 'f']),
            [
                'a' => 1,
                'd' => 4,
            ]);
    }

    public function test_without_other()
    {
        self::assertSame(array_without(
            [
                'a' => 1,
                'b' => 2,
                'c' => 3,
                'd' => 4,
                'e' => 5,
            ], ['f', 'g']),
            [
                'a' => 1,
                'b' => 2,
                'c' => 3,
                'd' => 4,
                'e' => 5,
            ]);
    }

    public function test_without_empty()
    {
        self::assertSame(array_without([], ['f', 'g']), []);
    }

    public function test_without_none()
    {
        self::assertSame(array_without(
            [
                'a' => 1,
                'b' => 2,
                'c' => 3,
                'd' => 4,
                'e' => 5,
            ], []),
            [
                'a' => 1,
                'b' => 2,
                'c' => 3,
                'd' => 4,
                'e' => 5,
            ]);
    }

    public function test_without_double()
    {
        self::assertSame(array_without(
            [
                'a' => 1,
                'b' => 2,
                'c' => 3,
                'd' => 4,
                'e' => 5,
            ], ['a', 'a']),
            [
                'b' => 2,
                'c' => 3,
                'd' => 4,
                'e' => 5,
            ]);
    }
}
