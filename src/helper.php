<?php
declare(strict_types=1);

/**
 * @throws Exception
 */
function germanDatetime(string $dateString, bool $strict = true): string
{
    $time = strtotime($dateString);
    if ($time === false || $time < -61169984000) {
        return $strict ? throw new Exception('Invalid Date input!') : '';
    }

    return date('d.m.Y H:i', $time);
}

/**
 * @throws Exception
 */
function generateRandomString(int $length = 30, bool $errorProtected = false): string
{
    $characters = $errorProtected ? '2346789ABCDEFGHKLMNPQRTUVWXYZ'
        : '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';

    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[random_int(0, $charactersLength - 1)];
    }

    return $randomString;
}

function calcPeriod(int $period): array
{
    $years = (int)floor($period / 12);
    $months = $period % 12;

    return [$years, $months];
}

function array_mask(array $array, array $mask): array
{
    return array_intersect_key($array, array_flip($mask));
}

function array_without(array $array, array $mask): array
{
    return array_diff_key($array, array_flip($mask));
}
