# Helper Functions for Karls

These functions are helper functions for our company.

## germanDatetime()

#### (string $dateString, bool $strict = true): string

This function transforms any given string which contains date information to a valid german datetime.

`string $dateString`

This is the date string which has to be converted.

`bool $strict = true`

This boolean decides if there should be an exception, or an empty string as return value.

`return string | Exception`

In case of invalid data there will be an exception or an empty string in non-strict mode.

### Example

> result: 01.12.2022 09:00
> 
> ($dateString = 2022-12-01 09:00, $strict = true)

## generateRandomString

#### (int $length = 30, bool $errorProtected = false): string

This function generates a random string with specific length and if needed error protected. This means, all ambiguous chars aren't included.

`int $length = 30`

The wanted length of the generated string.

`bool $errorProtectes = false`

This bool decides if there has no ambiguous chars in the random string.

`return string | Exception`

In case of invalid data there will be an exception.

### Example

> result: "4A65ushf4HzIUrw"
>
>($length = 15 && $errorProtected = false)

## calcPeriod

#### (int $period): array

This function calculates the years and months of a given month amount. 

`int $period`

The amount of months to calculate with. 

`return array`

This array consists of the amount of years and months calculated as numeric array [year, month].

### Example
> result: [4,11]
> 
> ($period = 59)